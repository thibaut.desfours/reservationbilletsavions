package com.rba.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class BilletRepository {
	private Connection conn;	
	
	public BilletRepository() {
		this.conn = ConnexionBDD.connexion();
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void close() {
		try {
			this.getConn().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Billet> getAll(){
		ReservationRepository reservationR = new ReservationRepository();
		VolRepository volR = new VolRepository();
		ClasseRepository classeR = new ClasseRepository();
		BaremeRepository baremeR = new BaremeRepository();
		ArrayList<Billet> billets = new ArrayList<Billet>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from billet;");
			ResultSet rs = statement.executeQuery();
			while(rs.next())
				billets.add(new Billet(rs.getInt("billet_id"),rs.getString("nom"),rs.getString("prenom"), rs.getString("civilite"), rs.getBoolean("bagage"), reservationR.getById(rs.getInt("reservation_id")),volR.getById(rs.getInt("vol_id")),classeR.getById(rs.getInt("classe_id")),baremeR.getById(rs.getInt("bareme_id"))));
			rs.close();
			statement.close();
			volR.close();
			classeR.close();
			baremeR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return billets;
	}
	
	public ArrayList<Billet> getAllByReservationId(int id){
		ReservationRepository reservationR = new ReservationRepository();
		VolRepository volR = new VolRepository();
		ClasseRepository classeR = new ClasseRepository();
		BaremeRepository baremeR = new BaremeRepository();
		ArrayList<Billet> billets = new ArrayList<Billet>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from billet where reservation_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			while(rs.next())
				billets.add(new Billet(rs.getInt("billet_id"),rs.getString("nom"),rs.getString("prenom"), rs.getString("civilite"), rs.getBoolean("bagage"),null,volR.getById(rs.getInt("vol_id")),classeR.getById(rs.getInt("classe_id")),baremeR.getById(rs.getInt("bareme_id"))));
			rs.close();
			statement.close();
			volR.close();
			classeR.close();
			baremeR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return billets;
	}
	
	public Billet getById(int id){
		ReservationRepository reservationR = new ReservationRepository();
		VolRepository volR = new VolRepository();
		ClasseRepository classeR = new ClasseRepository();
		BaremeRepository baremeR = new BaremeRepository();
		Billet billet = null;
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from billet where billet_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
				billet = new Billet(rs.getInt("billet_id"),rs.getString("nom"),rs.getString("prenom"), rs.getString("civilite"), rs.getBoolean("bagage"), reservationR.getById(rs.getInt("reservation_id")),volR.getById(rs.getInt("vol_id")),classeR.getById(rs.getInt("classe_id")),baremeR.getById(rs.getInt("bareme_id")));
			rs.close();
			statement.close();
			volR.close();
			classeR.close();
			baremeR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return billet;
	}
	
	public boolean add(ArrayList<Billet> billets) {
		int i =0;
		int size = billets.size();
		if(size < 1)
			return false;
		String sql = "insert into billet values ";
		for(Billet billet : billets)
			sql += "(null,?,?,?,?,?,?,?,?),";
		sql = sql.substring(0,sql.length()-1)+";";
		try {
			PreparedStatement statement = this.getConn().prepareStatement(sql);
			for(Billet billet : billets) {
				statement.setString(i=i+1, billet.getNom());	
				statement.setString(i=i+1, billet.getPrenom());
				statement.setString(i=i+1, billet.getCivilite());
				statement.setBoolean(i=i+1, billet.isBagage());
				statement.setInt(i=i+1, billet.getReservation().getId());
				statement.setInt(i=i+1, billet.getVol().getId());
				statement.setInt(i=i+1, billet.getClasse().getId());
				statement.setInt(i=i+1, billet.getBareme().getId());
			}
			if(statement.executeUpdate() < size) {
				statement.close();
				return false;
			}
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return true;
	}
}
