package com.rba.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AeroportRepository {
	private Connection conn;
	
	public AeroportRepository() {
		this.conn = ConnexionBDD.connexion();
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void close() {
		try {
			this.getConn().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Aeroport> getAll(){
		ArrayList<Aeroport> aeroports = new ArrayList<Aeroport>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from aeroport;");
			ResultSet rs = statement.executeQuery();
			while(rs.next())
				aeroports.add(new Aeroport(rs.getInt("aeroport_id"),rs.getString("pays"),rs.getString("ville"),rs.getString("nom")));
			rs.close();
			statement.close();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return aeroports;
	}
	
	public Aeroport getById(int id){
		Aeroport aeroport = null;
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from aeroport where aeroport_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
				aeroport = new Aeroport(rs.getInt("aeroport_id"),rs.getString("pays"),rs.getString("ville"),rs.getString("nom"));
			rs.close();
			statement.close();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return aeroport;
	}
}
