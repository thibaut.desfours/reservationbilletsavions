package com.rba.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VolRepository {
	private Connection conn;
	
	public VolRepository() {
		this.conn = ConnexionBDD.connexion();
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void close() {
		try {
			this.getConn().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Vol> getAll(){
		AvionRepository avionR = new AvionRepository();
		AeroportRepository aeroportR = new AeroportRepository();
		ArrayList<Vol> vols = new ArrayList<Vol>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from vol;");
			ResultSet rs = statement.executeQuery();
			while(rs.next())
				vols.add(new Vol(rs.getInt("vol_id"),rs.getFloat("prix_base"),rs.getString("dateD"),rs.getString("dateA"),avionR.getById(rs.getInt("avion_id")),aeroportR.getById(rs.getInt("aeroport_id_d")),aeroportR.getById(rs.getInt("aeroport_id_a"))));
			rs.close();
			statement.close();
			avionR.close();
			aeroportR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vols;
	}
	
	public ArrayList<Vol> getAllBySearchAS(int nbpassagers, int aeroportD, int aeroportA, String dateD){
		AvionRepository avionR = new AvionRepository();
		AeroportRepository aeroportR = new AeroportRepository();
		ArrayList<Vol> vols = new ArrayList<Vol>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select distinct v.* \r\n" + 
					"from vol v\r\n" + 
					"join aeroport aD on v.AEROPORT_ID_D = aD.AEROPORT_ID\r\n" + 
					"join aeroport aA on v.AEROPORT_ID_A = aA.AEROPORT_ID\r\n" + 
					"join avion a on v.AVION_ID = a.AVION_ID\r\n" + 
					"join possede p on a.AVION_ID = p.AVION_ID\r\n" + 
					"where \r\n" + 
					"		aD.AEROPORT_ID = ?\r\n" + 
					"	and aA.AEROPORT_ID = ?\r\n" + 
					"	and date_format(v.DATED,'%Y-%m-%d') = ?\r\n" + 
					"having \r\n" + 
					"	(\r\n" + 
					"		select sum(NBPLACES) - (select count(*) from billet where VOL_ID = v.VOL_ID) from vol join avion using(avion_id) join possede using(avion_id) where VOL_ID = v.VOL_ID\r\n" + 
					"	) >= ? order by v.prix_base;");
			statement.setInt(1, aeroportD);
			statement.setInt(2, aeroportA);
			statement.setString(3, dateD);
			statement.setInt(4, nbpassagers);
			ResultSet rs = statement.executeQuery();
			while(rs.next())
				vols.add(new Vol(rs.getInt("vol_id"),rs.getFloat("prix_base"),(String)rs.getString("dateD"),(String)rs.getString("dateA"),avionR.getById(rs.getInt("avion_id")),aeroportR.getById(rs.getInt("aeroport_id_d")),aeroportR.getById(rs.getInt("aeroport_id_a"))));
			rs.close();
			statement.close();
			avionR.close();
			aeroportR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vols;
	}
	
	public Vol getById(int id){
		AvionRepository avionR = new AvionRepository();
		AeroportRepository aeroportR = new AeroportRepository();
		Vol vol = null;
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from vol where vol_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
				vol = new Vol(rs.getInt("vol_id"),rs.getFloat("prix_base"),rs.getString("dateD"),rs.getString("dateA"),avionR.getById(rs.getInt("avion_id")),aeroportR.getById(rs.getInt("aeroport_id_d")),aeroportR.getById(rs.getInt("aeroport_id_a")));
			rs.close();
			statement.close();
			avionR.close();
			aeroportR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vol;
	}
}
