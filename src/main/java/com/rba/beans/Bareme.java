package com.rba.beans;

public class Bareme {
	private int id;
	private String libelle;
	private int ageD;
	private int ageF;
	private float coefficient;
	
	public Bareme(int id, String libelle, int ageD, int ageF, float coefficient) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.ageD = ageD;
		this.ageF = ageF;
		this.coefficient = coefficient;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getAgeD() {
		return ageD;
	}

	public void setAgeD(int ageD) {
		this.ageD = ageD;
	}

	public int getAgeF() {
		return ageF;
	}

	public void setAgeF(int ageF) {
		this.ageF = ageF;
	}

	public float getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(float coefficient) {
		this.coefficient = coefficient;
	}
}
