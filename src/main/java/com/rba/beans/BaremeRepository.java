package com.rba.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BaremeRepository {
	private Connection conn;	
	
	public BaremeRepository() {
		this.conn = ConnexionBDD.connexion();
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void close() {
		try {
			this.getConn().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Bareme> getAll(){
		ArrayList<Bareme> baremes = new ArrayList<Bareme>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from bareme;");
			ResultSet rs = statement.executeQuery();
			while(rs.next())
				baremes.add(new Bareme(rs.getInt("bareme_id"),rs.getString("libelle"),rs.getInt("ageD"),rs.getInt("ageF"),rs.getFloat("coefficient")));
			rs.close();
			statement.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return baremes;
	}
	
	public Bareme getById(int id){
		Bareme bareme = null;
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from bareme where bareme_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
				bareme = new Bareme(rs.getInt("bareme_id"),rs.getString("libelle"),rs.getInt("ageD"),rs.getInt("ageF"),rs.getFloat("coefficient"));
			rs.close();
			statement.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return bareme;
	}
}
