package com.rba.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class AvionRepository {
	private Connection conn;
	
	public AvionRepository() {
		this.conn = ConnexionBDD.connexion();
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void close() {
		try {
			this.getConn().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Avion> getAll(){
		ArrayList<Avion> avions = new ArrayList<Avion>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from avion;");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				HashMap<Classe,Integer> places = new HashMap<Classe, Integer>();
				PreparedStatement statement2 = this.getConn().prepareStatement("select classe_id, designation, coefficient, nbplaces from classe join possede using(classe_id) where possede.avion_id = ?;");
				statement2.setInt(1, rs.getInt("avion_id"));
				ResultSet rs2 = statement2.executeQuery();
				while(rs2.next()) {
					places.put(new Classe(rs2.getInt("classe_id"),rs2.getString("designation"),rs2.getFloat("coefficient")),rs2.getInt("nbplaces"));
				}
				avions.add(new Avion(rs.getInt("avion_id"),rs.getString("marque"),rs.getString("modele"),places));
				rs2.close();
				statement2.close();
			}
			rs.close();
			statement.close();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return avions;
	}
	
	public Avion getById(int id){
		Avion avion = null;
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from avion where avion_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				HashMap<Classe,Integer> places = new HashMap<Classe, Integer>();
				PreparedStatement statement2 = this.getConn().prepareStatement("select classe_id, designation, coefficient, nbplaces from classe join possede using(classe_id) where possede.avion_id = ?;");
				statement2.setInt(1, id);
				ResultSet rs2 = statement2.executeQuery();
				while(rs2.next()) {
					places.put(new Classe(rs2.getInt("classe_id"),rs2.getString("designation"),rs2.getFloat("coefficient")),rs2.getInt("nbplaces"));
				}
				avion = new Avion(rs.getInt("avion_id"),rs.getString("marque"),rs.getString("modele"),places);
				rs2.close();
				statement2.close();
			}
			rs.close();
			statement.close();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return avion;
	}
}
