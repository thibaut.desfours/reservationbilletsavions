package com.rba.beans;

public class Classe {
	private int id;
	private String designation;
	private float coefficient;
	
	public Classe(int id, String designation, float coefficient) {
		super();
		this.id = id;
		this.designation = designation;
		this.coefficient = coefficient;
	}

	public int getId() {
		return id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public float getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(float coefficient) {
		this.coefficient = coefficient;
	}
}
