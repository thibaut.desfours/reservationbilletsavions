package com.rba.beans;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnexionBDD {
	public static Connection connexion() {
		try {/*
			Properties prop = new Properties();
			InputStream input = new FileInputStream("src/main/resources/config.properties");
			prop.load(input);
			
			String url = prop.getProperty("db.url");
			String user = prop.getProperty("db.user");
			String password = prop.getProperty("db.password");
			*/
			Class.forName("com.mysql.cj.jdbc.Driver");
	        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/reservationbilletsavions?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC","root","");
	        return conn;			
        } catch (Exception ex) {
        	System.out.println(ex.getMessage());
            return null;
        }
	}
}
