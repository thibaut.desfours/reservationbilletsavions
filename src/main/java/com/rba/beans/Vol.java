package com.rba.beans;


public class Vol {
	private int id;
	private float prix_base;
	private String dateD;
	private String dateA;
	private Avion avion;
	private Aeroport aeroportD;
	private Aeroport aeroportA;
	
	public Vol(int id, float prix_base, String dateD, String dateA, Avion avion, Aeroport aeroportD,
			Aeroport aeroportA) {
		super();
		this.id = id;
		this.prix_base = prix_base;
		this.dateD = dateD;
		this.dateA = dateA;
		this.avion = avion;
		this.aeroportD = aeroportD;
		this.aeroportA = aeroportA;
	}

	public int getId() {
		return id;
	}

	public float getPrix_base() {
		return prix_base;
	}

	public void setPrix_base(float prix_base) {
		this.prix_base = prix_base;
	}

	public String getDateD() {
		return dateD;
	}

	public void setDateD(String dateD) {
		this.dateD = dateD;
	}

	public String getDateA() {
		return dateA;
	}

	public void setDateA(String dateA) {
		this.dateA = dateA;
	}

	public Avion getAvion() {
		return avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	public Aeroport getAeroportD() {
		return aeroportD;
	}

	public void setAeroportD(Aeroport aeroportD) {
		this.aeroportD = aeroportD;
	}

	public Aeroport getAeroportA() {
		return aeroportA;
	}

	public void setAeroportA(Aeroport aeroportA) {
		this.aeroportA = aeroportA;
	}
}
