package com.rba.beans;

import java.util.ArrayList;
import java.util.Date;

public class Reservation {
	private int id;
	private String dateR;
	private String type;
	private String mail;
	private String tel;
	private String nom;
	private String prenom;
	private ArrayList<Billet> billets;
	
	public Reservation(int id, String dateR, String type, String mail, String tel, String nom, String prenom,
			ArrayList<Billet> billets) {
		super();
		this.id = id;
		this.dateR = dateR;
		this.type = type;
		this.mail = mail;
		this.tel = tel;
		this.nom = nom;
		this.prenom = prenom;
		this.billets = billets;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getDateR() {
		return dateR;
	}

	public void setDateR(String dateR) {
		this.dateR = dateR;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public ArrayList<Billet> getBillets() {
		return billets;
	}

	public void setBillets(ArrayList<Billet> billets) {
		this.billets = billets;
	}
}
