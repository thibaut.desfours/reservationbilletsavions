package com.rba.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClasseRepository {
	private Connection conn;
	
	public ClasseRepository() {
		this.conn = ConnexionBDD.connexion();
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void close() {
		try {
			this.getConn().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Classe> getAll(){
		ArrayList<Classe> classes = new ArrayList<Classe>();
		PreparedStatement statement;
		try {
			statement = this.getConn().prepareStatement("select * from classe;");
			ResultSet rs = statement.executeQuery();
			while(rs.next())
				classes.add(new Classe(rs.getInt("classe_id"),rs.getString("designation"),rs.getFloat("coefficient")));
			rs.close();
			statement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return classes;
	}
	
	public Classe getById(int id){
		Classe classe = null;
		PreparedStatement statement;
		try {
			statement = this.getConn().prepareStatement("select * from classe where classe_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
				classe = new Classe(rs.getInt("classe_id"),rs.getString("designation"),rs.getFloat("coefficient"));
			rs.close();
			statement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return classe;
	}
	
}
