package com.rba.beans;

import java.util.Date;

public class Billet {
	private int id;
	private String nom;
	private String prenom;
	private String civilite;
	private boolean bagage;
	private Reservation reservation;
	private Vol vol;
	private Classe classe;
	private Bareme bareme;
	
	public Billet(int id, String nom, String prenom, String civilite, boolean bagage, Reservation reservation, Vol vol,
			Classe classe, Bareme bareme) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.civilite = civilite;
		this.bagage = bagage;
		this.reservation = reservation;
		this.vol = vol;
		this.classe = classe;
		this.bareme = bareme;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	
	public boolean isBagage() {
		return bagage;
	}

	public void setBagage(boolean bagage) {
		this.bagage = bagage;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Vol getVol() {
		return vol;
	}

	public void setVol(Vol vol) {
		this.vol = vol;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public Bareme getBareme() {
		return bareme;
	}

	public void setBareme(Bareme bareme) {
		this.bareme = bareme;
	}

	public void setId(int id) {
		this.id = id;
	}
}
