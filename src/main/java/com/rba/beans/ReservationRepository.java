package com.rba.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ReservationRepository {
	private Connection conn;
		
	public ReservationRepository() {
		this.conn = ConnexionBDD.connexion();
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void close() {
		try {
			this.getConn().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Reservation> getAll() {
		ClasseRepository classeR = new ClasseRepository();
		VolRepository volR = new VolRepository();
		BilletRepository billetR = new BilletRepository();
		ArrayList<Reservation> reservations = new ArrayList<Reservation>();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from reservation;");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) 
				reservations.add(new Reservation(rs.getInt("reservation_id"),rs.getString("dateR"),rs.getString("type"),rs.getString("mail"),rs.getString("tel"),rs.getString("nom"),rs.getString("prenom"),billetR.getAllByReservationId(rs.getInt("reservation_id"))));
			rs.close();
			statement.close();
			volR.close();
			classeR.close();
			billetR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}		
		return reservations;
	}
	
	public Reservation getById(int id) {
		ClasseRepository classeR = new ClasseRepository();
		VolRepository volR = new VolRepository();
		BilletRepository billetR = new BilletRepository();
		Reservation reservation = null;
		try {
			PreparedStatement statement = this.getConn().prepareStatement("select * from reservation where reservation_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				reservation = new Reservation(rs.getInt("reservation_id"),rs.getString("dateR"),rs.getString("type"),rs.getString("mail"),rs.getString("tel"),rs.getString("nom"),rs.getString("prenom"),billetR.getAllByReservationId(rs.getInt("reservation_id")));
			}
			rs.close();
			statement.close();
			volR.close();
			classeR.close();
			billetR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}		
		return reservation;
	}
	
	public boolean add(Reservation reservation) {
		BilletRepository billetR = new BilletRepository();
		try {
			PreparedStatement statement = this.getConn().prepareStatement("insert into reservation values(null,?,?,?,?,?,?);");
			statement.setString(1, reservation.getDateR());
			statement.setString(2, reservation.getType());
			statement.setString(3, reservation.getMail());
			statement.setString(4, reservation.getTel());
			statement.setString(5, reservation.getNom());
			statement.setString(6, reservation.getPrenom());
			
			if(statement.executeUpdate() < 1)
				return false;
			
			PreparedStatement statement2 = this.getConn().prepareStatement("select max(reservation_id) as id from reservation;");
			ResultSet rs2 = statement2.executeQuery();
			if(rs2.next()) {			
				ArrayList<Billet> billets = new ArrayList<Billet>();
				billets.addAll(reservation.getBillets());
				for(int i = 0; i < reservation.getBillets().size(); i++)
					billets.get(i).getReservation().setId(rs2.getInt("id"));
				reservation.setBillets(billets);
				billetR.add(reservation.getBillets());
			}
			statement.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
