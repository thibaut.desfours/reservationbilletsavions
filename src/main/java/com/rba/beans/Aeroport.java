package com.rba.beans;

public class Aeroport {
	private int id;
	private String pays;
	private String ville;
	private String nom;
	
	public Aeroport(int id, String pays, String ville, String nom) {
		super();
		this.id = id;
		this.pays = pays;
		this.ville = ville;
		this.nom = nom;
	}

	public int getId() {
		return id;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}	
}
