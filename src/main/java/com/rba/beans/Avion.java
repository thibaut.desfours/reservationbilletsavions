package com.rba.beans;

import java.util.HashMap;

public class Avion {
	private int id;
	private String marque;
	private String modele;
	private HashMap<Classe,Integer> places;
	
	public Avion(int id, String marque, String modele, HashMap<Classe, Integer> places) {
		super();
		this.id = id;
		this.marque = marque;
		this.modele = modele;
		this.places = places;
	}

	public int getId() {
		return id;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public HashMap<Classe, Integer> getPlaces() {
		return places;
	}

	public void setClasses(HashMap<Classe, Integer> places) {
		this.places = places;
	}
}
