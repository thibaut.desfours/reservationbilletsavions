package com.rba.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rba.beans.AeroportRepository;
import com.rba.beans.BaremeRepository;
import com.rba.beans.Billet;
import com.rba.beans.BilletRepository;
import com.rba.beans.ClasseRepository;
import com.rba.beans.Reservation;
import com.rba.beans.ReservationRepository;

public class Paiement extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int reservationId = 1;
		BilletRepository billetR = new BilletRepository();

		request.setAttribute("billets", billetR.getAllByReservationId(reservationId));
		this.getServletContext().getRequestDispatcher("/paiement.jsp").forward(request, response);
	}
}
