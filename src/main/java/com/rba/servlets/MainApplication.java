package com.rba.servlets;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.rba.beans.Bareme;
import com.rba.beans.BaremeRepository;
import com.rba.beans.Billet;
import com.rba.beans.Reservation;
import com.rba.beans.ReservationRepository;

public class MainApplication {
	public static void main(String[] args) {
		/*
		AeroportRepository aeroportR = new AeroportRepository();
		for(Aeroport aeroport : aeroportR.getAll())
			System.out.println(aeroport.getId()+" "+aeroport.getNom()+" "+aeroport.getVille()+" "+aeroport.getPays());
		Aeroport aeroport = aeroportR.getById(1);
		System.out.println(aeroport.getId()+" "+aeroport.getNom()+" "+aeroport.getVille()+" "+aeroport.getPays());
		
		AvionRepository avionR = new AvionRepository();
		for(Avion avion : avionR.getAll()) {
			System.out.println(avion.getId()+" "+avion.getMarque()+" "+avion.getModele()+" "+avion.getPlaces());
		}
		Avion avion = avionR.getById(1);
		System.out.println(avion.getId()+" "+avion.getMarque()+" "+avion.getModele()+" :");
		avion.getPlaces().forEach((classe,nbplaces) -> {
			System.out.println(" 	- "+classe.getDesignation()+" : "+nbplaces+" places");
		});
		
		ClasseRepository classeR = new ClasseRepository();
		for(Classe classe : classeR.getAll())
			System.out.println(classe.getId()+" "+classe.getDesignation()+" "+classe.getCoefficient());
		Classe classe = classeR.getById(1);
		System.out.println(classe.getId()+" "+classe.getDesignation()+" "+classe.getCoefficient());
		
		VolRepository volR = new VolRepository();
		for(Vol vol : volR.getAll())
			System.out.println(vol.getId()+" "+vol.getPrix_base()+" "+vol.getAeroportD().getNom()+" "+vol.getAeroportA().getNom()+" "+vol.getDateD()+" "+vol.getDateA()+" "+vol.getAvion().getModele());
		Vol vol = volR.getById(1);
		System.out.println(vol.getId()+" "+vol.getPrix_base()+" "+vol.getAeroportD().getNom()+" "+vol.getAeroportA().getNom()+" "+vol.getDateD()+" "+vol.getDateA()+" "+vol.getAvion().getModele());
		
		BilletRepository billetR = new BilletRepository();
		for(Billet billet : billetR.getAll())
			System.out.println(billet.getId()+" "+billet.getClasse().getDesignation()+" "+billet.getNom()+" "+billet.getPrenom()+" "+billet.getDateN()+" "+billet.getVol().getAeroportA().getNom());
		Billet billet1 = billetR.getById(1);
		System.out.println(billet1.getId()+" "+billet1.getClasse().getDesignation()+" "+billet1.getNom()+" "+billet1.getPrenom()+" "+billet1.getDateN()+" "+billet1.getVol().getAeroportA().getNom());
		 */
		ReservationRepository reservationR = new ReservationRepository();
		for(Reservation reservation : reservationR.getAll()) {
			System.out.println(reservation.getId()+" "+reservation.getMail()+" "+reservation.getTel()+" "+reservation.getDateR()+" "+reservation.getNom()+" "+reservation.getPrenom()+" "+reservation.getType()+" :");
			for(Billet billet : reservation.getBillets())
				System.out.println(" 	- "+billet.getId()+" "+billet.getClasse().getDesignation()+" "+billet.getNom()+" "+billet.getPrenom()+" "+billet.isBagage()+" "+billet.getVol().getAeroportA().getNom()+" "+billet.getBareme().getLibelle()+" "+billet.getVol().getDateA());
		}/*
		Reservation reservation = reservationR.getById(1);
		System.out.println(reservation.getId()+" "+reservation.getMail()+" "+reservation.getTel()+" "+reservation.getDateR()+" "+reservation.getNom()+" "+reservation.getPrenom()+" "+reservation.getType()+" :");
		for(Billet billet : reservation.getBillets())
			System.out.println(" 	- "+billet.getId()+" "+billet.getClasse().getDesignation()+" "+billet.getNom()+" "+billet.getPrenom()+" "+billet.getDateN()+" "+billet.getVol().getAeroportA().getNom());
		/*
		Reservation radd;
		try {
			radd = new Reservation(0, format.parse("2020-10-04"), "type", "mail", "tel", "nom", "prenom", new ArrayList<Billet>());
			reservationR.add(radd);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		BilletRepository billetR = new BilletRepository();
		ArrayList<Billet> billetsListe = billetR.getAll();
		for(Billet billet : billetsListe) {
			try {
				billet.setDateN(format.parse("2020-10-04"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		billetR.add(billetsListe);
		for(Billet billet : billetR.getAll())
			System.out.println(billet.getId()+" "+billet.getClasse().getDesignation()+" "+billet.getNom()+" "+billet.getPrenom()+" "+billet.getDateN()+" "+billet.getVol().getAeroportA().getNom());
		*/
		BaremeRepository baremeR = new BaremeRepository();
		for(Bareme bareme : baremeR.getAll())
			System.out.println(bareme.getId()+" "+bareme.getLibelle()+" "+bareme.getAgeD()+" "+bareme.getAgeF()+" "+bareme.getCoefficient());
		Bareme b = baremeR.getById(1);
		System.out.println(b.getId()+" "+b.getLibelle()+" "+b.getAgeD()+" "+b.getAgeF()+" "+b.getCoefficient());
	}
}
