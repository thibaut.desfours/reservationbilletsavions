package com.rba.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rba.beans.AeroportRepository;
import com.rba.beans.Bareme;
import com.rba.beans.BaremeRepository;
import com.rba.beans.Billet;
import com.rba.beans.ClasseRepository;
import com.rba.beans.Reservation;
import com.rba.beans.Vol;
import com.rba.beans.VolRepository;

public class Vols extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		VolRepository volR = new VolRepository();
	 	
		HttpSession session = request.getSession();
		
		Reservation reservation = (Reservation) session.getAttribute("reservation");
		
		Vol vol = volR.getById(Integer.parseInt(request.getParameter("volAller")));
		ArrayList<Bareme> baremes = new ArrayList<Bareme>();
		for(int i=0; i< reservation.getBillets().size();i++) {
			reservation.getBillets().get(i).setVol(vol);
			baremes.add(reservation.getBillets().get(i).getBareme());
		}
		session.setAttribute("reservation", reservation);
		
		request.setAttribute("aeroportD", vol.getAeroportA());
	    request.setAttribute("aeroportA", vol.getAeroportD());
	    request.setAttribute("vols", volR.getAllBySearchAS(reservation.getBillets().size(), vol.getAeroportA().getId(), vol.getAeroportD().getId(), (String) session.getAttribute("dateRetour")));
	    request.setAttribute("classe", reservation.getBillets().get(0).getClasse());
		request.setAttribute("baremes", baremes );
		request.setAttribute("typeVol", reservation.getType());
		request.setAttribute("volRetour", true);
		this.getServletContext().getRequestDispatcher("/vols.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		VolRepository volR = new VolRepository();
	    ClasseRepository classeR = new ClasseRepository();
	    BaremeRepository baremeR = new BaremeRepository();
	    AeroportRepository aeroportR = new AeroportRepository();
	    int nbpassagers = 0;
	    ArrayList<Bareme> baremes = new ArrayList<Bareme>();
	    for(int i=1; i<5;i++) {
	    	if(request.getParameter("person"+i)!=null) {
	    		nbpassagers++;
	    		baremes.add(baremeR.getById(Integer.parseInt(request.getParameter("person"+i))));
	    	}
	    }
		
		Reservation reservation = new Reservation(0,"2020-10-07",request.getParameter("typeVol"),"","","","",new ArrayList<Billet>());
	    ArrayList<Billet> billets = new ArrayList<Billet>();	    
		for(Bareme bareme : baremes) {
	    	billets.add(new Billet(0,"","","",false,null,null,classeR.getById(Integer.parseInt(request.getParameter("selectClasse"))),bareme));
	    }
		reservation.setBillets(billets);
		
		HttpSession session = request.getSession();
        session.setAttribute("reservation", reservation);
        session.setAttribute("dateRetour", request.getParameter("dateRetour"));
        
        request.setAttribute("aeroportD", aeroportR.getById(Integer.parseInt(request.getParameter("selectDepart"))));
	    request.setAttribute("aeroportA", aeroportR.getById(Integer.parseInt(request.getParameter("selectArrivee"))));
	    request.setAttribute("vols", volR.getAllBySearchAS(nbpassagers, Integer.parseInt(request.getParameter("selectDepart")), Integer.parseInt(request.getParameter("selectArrivee")), request.getParameter("dateDepart")));
	
	    request.setAttribute("classe", classeR.getById(Integer.parseInt(request.getParameter("selectClasse"))));
		request.setAttribute("baremes", baremes);
		request.setAttribute("typeVol",request.getParameter("typeVol"));
		this.getServletContext().getRequestDispatcher("/vols.jsp").forward(request, response);
	}		
}