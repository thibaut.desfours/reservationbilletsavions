package com.rba.servlets;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rba.beans.Billet;
import com.rba.beans.Reservation;


public class RecapitulatifReservation extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		Reservation reservation = (Reservation) session.getAttribute("reservation");
		ArrayList<Billet> billets = new ArrayList<Billet>();
		billets.addAll(reservation.getBillets());
		for(int i = 0; i < reservation.getBillets().size(); i++) {
			billets.get(i).setCivilite(request.getParameter("civilite"+i));
			billets.get(i).setNom(request.getParameter("nom"+i));
			billets.get(i).setPrenom(request.getParameter("prenom"+i));
			billets.get(i).setBagage(Boolean.parseBoolean(request.getParameter("bagages"+i)));
		}
		reservation.setBillets(billets);
		session.setAttribute("reservation", reservation);
	    this.getServletContext().getRequestDispatcher("/recapitulatifReservation.jsp").forward(request, response);
	}
}