package com.rba.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rba.beans.AeroportRepository;
import com.rba.beans.Bareme;
import com.rba.beans.BaremeRepository;
import com.rba.beans.ClasseRepository;
import com.rba.beans.VolRepository;

public class ReservationVol extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		ClasseRepository classeR = new ClasseRepository();
		AeroportRepository aeroportR = new AeroportRepository();
		BaremeRepository baremeR = new BaremeRepository();
		request.setAttribute("classes", classeR.getAll());
		request.setAttribute("aeroports", aeroportR.getAll());
		request.setAttribute("baremes", baremeR.getAll());
	    this.getServletContext().getRequestDispatcher("/reservation.jsp").forward(request, response);
	}	
}