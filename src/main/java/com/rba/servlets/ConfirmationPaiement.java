package com.rba.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rba.beans.AeroportRepository;
import com.rba.beans.Bareme;
import com.rba.beans.BaremeRepository;
import com.rba.beans.Billet;
import com.rba.beans.ClasseRepository;
import com.rba.beans.Reservation;
import com.rba.beans.ReservationRepository;
import com.rba.beans.VolRepository;

public class ConfirmationPaiement extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		ReservationRepository reservationR = new ReservationRepository();
		HttpSession session = request.getSession();
		Reservation reservation = (Reservation) session.getAttribute("reservation");
		reservation.setNom(request.getParameter("nom"));
		reservation.setPrenom(request.getParameter("prenom"));
		reservation.setMail(request.getParameter("email"));
		reservation.setTel(request.getParameter("tel"));
		session.setAttribute("reservation", reservation);
		
		ArrayList<Billet> billets = new ArrayList<Billet>();
		billets.addAll(reservation.getBillets());
		for(int i = 0; i < reservation.getBillets().size(); i++) {
			billets.get(i).setReservation(reservation);
		}
		reservation.setBillets(billets);
		reservationR.add(reservation);
		this.getServletContext().getRequestDispatcher("/confirmationPaiement.jsp").forward(request, response);
	}
}