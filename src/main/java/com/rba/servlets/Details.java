package com.rba.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rba.beans.AeroportRepository;
import com.rba.beans.Bareme;
import com.rba.beans.BaremeRepository;
import com.rba.beans.ClasseRepository;
import com.rba.beans.Reservation;
import com.rba.beans.Vol;
import com.rba.beans.VolRepository;

public class Details extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		VolRepository volR = new VolRepository();
		Reservation reservation = (Reservation) session.getAttribute("reservation");
		
		Vol vol = volR.getById(Integer.parseInt(request.getParameter("vol")));
		
	    request.setAttribute("vol", vol);
	    this.getServletContext().getRequestDispatcher("/details.jsp").forward(request, response);
	}
	
	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		this.getServletContext().getRequestDispatcher("/ajoutBillet.jsp").forward(request, response);
	}
}
