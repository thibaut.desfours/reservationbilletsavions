package com.rba.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rba.beans.Billet;
import com.rba.beans.Reservation;
import com.rba.beans.VolRepository;

public class AjoutBillet extends HttpServlet{
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		VolRepository volR = new VolRepository();
		HttpSession session = request.getSession();		
		Reservation reservation = (Reservation) session.getAttribute("reservation");
		ArrayList<Billet> billets = new ArrayList<Billet>();
		billets.addAll(reservation.getBillets());
		int i=0;
		for(Billet billet : reservation.getBillets()) {
			if(request.getParameter("volRetour") != null)
				billets.add(new Billet(0,"","","",false,null,volR.getById(Integer.parseInt(request.getParameter("volRetour"))),billet.getClasse(),billet.getBareme()));
			else if(request.getParameter("volAller") != null) {
				billets.set(i,new Billet(0,"","","",false,null,volR.getById(Integer.parseInt(request.getParameter("volAller"))),billet.getClasse(),billet.getBareme()));		
				i++;
			}
		}
		reservation.setBillets(billets);
		session.setAttribute("reservation", reservation);
		this.getServletContext().getRequestDispatcher( "/ajoutBillet.jsp").forward( request, response );
	}
}