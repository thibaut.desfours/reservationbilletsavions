-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 05 oct. 2020 à 18:07
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `reservationbilletsavions`
--

-- --------------------------------------------------------

--
-- Structure de la table `aeroport`
--

DROP TABLE IF EXISTS `aeroport`;
CREATE TABLE IF NOT EXISTS `aeroport` (
  `AEROPORT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAYS` varchar(60) NOT NULL,
  `VILLE` varchar(60) NOT NULL,
  `NOM` varchar(80) NOT NULL,
  PRIMARY KEY (`AEROPORT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `aeroport`
--

INSERT INTO `aeroport` (`AEROPORT_ID`, `PAYS`, `VILLE`, `NOM`) VALUES
(1, 'France', 'Paris', 'Aéroport Paris Charles de Gaulle'),
(2, 'États-Unis', 'New York', 'Aéroport John F. Kennedy'),
(3, 'Allemagne', 'Berlin', 'Aéroport de Berlin-Schönefeld'),
(4, 'Belgique', 'Bruxelles', 'Aéroport de Bruxelles-National'),
(5, 'Chine', 'Pékin', 'Aéroport international de Pékin-Capitale'),
(6, 'Japon', 'Tokyo', 'Aéroport de Tokyo');

-- --------------------------------------------------------

--
-- Structure de la table `avion`
--

DROP TABLE IF EXISTS `avion`;
CREATE TABLE IF NOT EXISTS `avion` (
  `AVION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MARQUE` varchar(60) NOT NULL,
  `MODELE` varchar(30) NOT NULL,
  PRIMARY KEY (`AVION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `avion`
--

INSERT INTO `avion` (`AVION_ID`, `MARQUE`, `MODELE`) VALUES
(1, 'Airbus', 'A300'),
(2, 'Airbus', 'A310'),
(3, 'Boeing', '737'),
(4, 'Boeing', '747');

-- --------------------------------------------------------

--
-- Structure de la table `bareme`
--

DROP TABLE IF EXISTS `bareme`;
CREATE TABLE IF NOT EXISTS `bareme` (
  `BAREME_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(80) NOT NULL,
  `AGED` int(3) NOT NULL,
  `AGEF` int(3) NOT NULL,
  `COEFFICIENT` float NOT NULL,
  PRIMARY KEY (`BAREME_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `bareme`
--

INSERT INTO `bareme` (`BAREME_ID`, `LIBELLE`, `AGED`, `AGEF`, `COEFFICIENT`) VALUES
(1, 'Enfant (2 - 11 ans inclus)', 2, 11, 0.25),
(2, 'Jeune (12 - 17 ans inclus)', 12, 17, 0.75),
(3, 'Jeune (18 - 24 ans inclus)', 18, 24, 0.85),
(4, 'Etudiant (18 - 29 ans inclus)', 18, 29, 0.5),
(5, 'Adulte (25 - 64 ans inclus)', 25, 64, 1),
(6, 'Sénior (65 ans et plus)', 65, 150, 0.75),
(7, 'Enfant (moins de 2ans)', 0, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `billet`
--

DROP TABLE IF EXISTS `billet`;
CREATE TABLE IF NOT EXISTS `billet` (
  `BILLET_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(30) NOT NULL,
  `PRENOM` varchar(30) NOT NULL,
  `CIVILITE` varchar(1) NOT NULL,
  `BAGAGE` tinyint(1) NOT NULL,
  `RESERVATION_ID` int(11) NOT NULL,
  `VOL_ID` int(11) NOT NULL,
  `CLASSE_ID` int(11) NOT NULL,
  `BAREME_ID` int(11) NOT NULL,
  PRIMARY KEY (`BILLET_ID`),
  KEY `BILLET_RESERVATION_FK` (`RESERVATION_ID`),
  KEY `BILLET_VOL0_FK` (`VOL_ID`),
  KEY `BILLET_CLASSE1_FK` (`CLASSE_ID`),
  KEY `billet_FK` (`BAREME_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `billet`
--

INSERT INTO `billet` (`BILLET_ID`, `NOM`, `PRENOM`, `CIVILITE`, `BAGAGE`, `RESERVATION_ID`, `VOL_ID`, `CLASSE_ID`, `BAREME_ID`) VALUES
(1, 'DESFOURS', 'Thibaut', 'M', 1, 1, 1, 4, 5),
(2, 'DESFOURS', 'Thibaut', 'M', 1, 1, 4, 4, 5),
(3, 'NGUYEN', 'Michaël', 'M', 0, 1, 1, 4, 1),
(4, 'NGUYEN', 'Michaël', 'M', 0, 1, 4, 4, 1),
(5, 'GÉMY', 'Dorian', 'M', 1, 2, 2, 3, 3),
(6, 'DOPPLER', 'Quentin', 'F', 0, 3, 3, 1, 6),
(7, 'GROSDOIGT', 'Quentin', 'M', 1, 3, 3, 1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `CLASSE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESIGNATION` varchar(60) NOT NULL,
  `COEFFICIENT` float NOT NULL,
  PRIMARY KEY (`CLASSE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`CLASSE_ID`, `DESIGNATION`, `COEFFICIENT`) VALUES
(1, 'Économique', 1),
(2, 'Premium', 1.25),
(3, 'Affaires', 1.75),
(4, 'Première', 2);

-- --------------------------------------------------------

--
-- Structure de la table `possede`
--

DROP TABLE IF EXISTS `possede`;
CREATE TABLE IF NOT EXISTS `possede` (
  `AVION_ID` int(11) NOT NULL,
  `CLASSE_ID` int(11) NOT NULL,
  `NBPLACES` int(11) NOT NULL,
  PRIMARY KEY (`AVION_ID`,`CLASSE_ID`),
  KEY `POSSEDE_CLASSE0_FK` (`CLASSE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `possede`
--

INSERT INTO `possede` (`AVION_ID`, `CLASSE_ID`, `NBPLACES`) VALUES
(1, 1, 100),
(1, 2, 100),
(1, 3, 75),
(1, 4, 48),
(2, 1, 100),
(2, 2, 125),
(2, 3, 25),
(2, 4, 25),
(3, 1, 125),
(3, 2, 115),
(3, 3, 30),
(3, 4, 25),
(4, 1, 115),
(4, 2, 100),
(4, 3, 75),
(4, 4, 50);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `RESERVATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATER` date NOT NULL,
  `TYPE` varchar(60) NOT NULL,
  `MAIL` varchar(60) NOT NULL,
  `TEL` varchar(12) NOT NULL,
  `NOM` varchar(30) NOT NULL,
  `PRENOM` varchar(30) NOT NULL,
  PRIMARY KEY (`RESERVATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`RESERVATION_ID`, `DATER`, `TYPE`, `MAIL`, `TEL`, `NOM`, `PRENOM`) VALUES
(1, '2020-10-11', 'Aller et retour', 'thibaut.desfours@viacesi.fr', '0652648595', 'DESFOURS', 'Thibaut'),
(2, '2020-10-05', 'Aller simple', 'dorian.gemy@viacesi.fr', '0685978457', 'GÉMY', 'Dorian'),
(3, '2020-10-15', 'Aller simple', 'quentin.doppler@viacesi.fr', '0658978547', 'DOPPLER', 'Quentin');

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

DROP TABLE IF EXISTS `vol`;
CREATE TABLE IF NOT EXISTS `vol` (
  `VOL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRIX_BASE` float NOT NULL,
  `DATED` timestamp NOT NULL,
  `DATEA` timestamp NOT NULL,
  `AVION_ID` int(11) NOT NULL,
  `AEROPORT_ID_D` int(11) NOT NULL,
  `AEROPORT_ID_A` int(11) NOT NULL,
  PRIMARY KEY (`VOL_ID`),
  KEY `VOL_AVION_FK` (`AVION_ID`),
  KEY `VOL_AEROPORT0_FK` (`AEROPORT_ID_D`),
  KEY `VOL_AEROPORT1_FK` (`AEROPORT_ID_A`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vol`
--

INSERT INTO `vol` (`VOL_ID`, `PRIX_BASE`, `DATED`, `DATEA`, `AVION_ID`, `AEROPORT_ID_D`, `AEROPORT_ID_A`) VALUES
(1, 400, '2020-10-14 18:21:36', '2020-10-15 06:10:36', 1, 1, 2),
(2, 100, '2020-10-12 06:30:00', '2020-10-12 09:00:00', 1, 4, 1),
(3, 500, '2020-10-22 18:25:05', '2020-10-23 10:00:00', 2, 6, 2),
(4, 400, '2020-10-22 18:37:25', '2020-10-23 08:37:25', 3, 2, 1),
(5, 400, '2020-10-14 20:21:36', '2020-10-15 08:10:36', 1, 1, 2),
(6, 100, '2020-10-12 08:30:00', '2020-10-12 11:00:00', 1, 4, 1),
(7, 500, '2020-10-22 20:25:05', '2020-10-23 12:00:00', 2, 6, 2),
(8, 400, '2020-10-22 20:37:25', '2020-10-23 10:37:25', 3, 2, 1),
(9, 500, '2020-10-28 16:09:08', '2020-11-12 09:35:00', 2, 3, 5),
(10, 700, '2020-10-06 16:14:56', '2020-10-17 12:35:00', 3, 1, 2),
(11, 350, '2020-10-15 09:15:00', '2020-10-15 17:30:00', 4, 3, 1),
(12, 450, '2020-10-14 10:30:00', '2020-10-15 13:00:00', 3, 1, 2),
(13, 325, '2020-10-14 06:15:00', '2020-10-15 10:00:00', 2, 1, 2),
(14, 435, '2020-10-14 15:30:00', '2020-10-15 22:00:00', 4, 1, 2),
(15, 600, '2020-10-14 12:30:00', '2020-10-15 19:30:00', 1, 1, 2),
(16, 400, '2020-10-22 09:45:00', '2020-10-23 19:30:00', 4, 2, 1),
(17, 520, '2020-10-22 10:00:00', '2020-10-23 20:30:00', 1, 2, 1),
(18, 480, '2020-10-22 17:20:00', '2020-10-23 23:00:00', 2, 2, 1),
(19, 320, '2020-10-22 22:30:00', '2020-10-23 23:00:00', 3, 2, 1);
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `billet`
--
ALTER TABLE `billet`
  ADD CONSTRAINT `BILLET_CLASSE1_FK` FOREIGN KEY (`CLASSE_ID`) REFERENCES `classe` (`CLASSE_ID`),
  ADD CONSTRAINT `BILLET_RESERVATION_FK` FOREIGN KEY (`RESERVATION_ID`) REFERENCES `reservation` (`RESERVATION_ID`),
  ADD CONSTRAINT `BILLET_VOL0_FK` FOREIGN KEY (`VOL_ID`) REFERENCES `vol` (`VOL_ID`),
  ADD CONSTRAINT `billet_FK` FOREIGN KEY (`BAREME_ID`) REFERENCES `bareme` (`BAREME_ID`);

--
-- Contraintes pour la table `possede`
--
ALTER TABLE `possede`
  ADD CONSTRAINT `POSSEDE_AVION_FK` FOREIGN KEY (`AVION_ID`) REFERENCES `avion` (`AVION_ID`),
  ADD CONSTRAINT `POSSEDE_CLASSE0_FK` FOREIGN KEY (`CLASSE_ID`) REFERENCES `classe` (`CLASSE_ID`);

--
-- Contraintes pour la table `vol`
--
ALTER TABLE `vol`
  ADD CONSTRAINT `VOL_AEROPORT0_FK` FOREIGN KEY (`AEROPORT_ID_D`) REFERENCES `aeroport` (`AEROPORT_ID`),
  ADD CONSTRAINT `VOL_AEROPORT1_FK` FOREIGN KEY (`AEROPORT_ID_A`) REFERENCES `aeroport` (`AEROPORT_ID`),
  ADD CONSTRAINT `VOL_AVION_FK` FOREIGN KEY (`AVION_ID`) REFERENCES `avion` (`AVION_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
