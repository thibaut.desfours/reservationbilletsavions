<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.rba.beans.Classe" %>
<%@ page import="com.rba.beans.Aeroport" %>
<%@ page import="com.rba.beans.Bareme" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="css/jquery-ui.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/reservation.css"/>
  <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>  
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <title>Réservation de vols</title>
</head>

<body>
	<%@include file="header.jsp" %>
  <div id="searchBox" class="ui-widget">
  	<form method="post" action="vols">
	    <div id="searchBoxContent">
	      <h2 id="titreSearchBox">Acheter un billet</h2>
	      <div id="divTypeVol">
	        <div>
	          <input type="radio" class="radio" id="allerSimple" name="typeVol" value="Aller simple">
	          <label for="huey">Aller simple</label>
	        </div>
	        <div>
	          <input type="radio" class="radio" id="allerRetour" name="typeVol" value="Aller-retour" checked>
	          <label for="dewey">Aller-retour</label>
	        </div>
	      </div>
	      <div id="divInputDepart" class="form-group col-md-6">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M2.5,19H21.5V21H2.5V19M22.07,9.64C21.86,8.84 21.03,8.36 20.23,8.58L14.92,10L8,3.57L6.09,4.08L10.23,11.25L5.26,12.58L3.29,11.04L1.84,11.43L3.66,14.59L4.43,15.92L6.03,15.5L11.34,14.07L15.69,12.91L21,11.5C21.81,11.26 22.28,10.44 22.07,9.64Z" />
	        </svg>
	        <select name="selectDepart" class="form-control form-control-sm">
	        	<%
	        		ArrayList<Aeroport> aeroports = (ArrayList<Aeroport>) request.getAttribute("aeroports");
	        		out.println("<option selected disabled>Départ de</option>");
	        		for(Aeroport aeroport : aeroports){
	        			out.println("<option value='"+aeroport.getId()+"'>"+aeroport.getNom()+"</option>");
	        		}
	        	%>
	        </select>
	      </div>
	      <div id="divInputArrivee" class="form-group col-md-6">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M2.5,19H21.5V21H2.5V19M9.68,13.27L14.03,14.43L19.34,15.85C20.14,16.06 20.96,15.59 21.18,14.79C21.39,14 20.92,13.17 20.12,12.95L14.81,11.53L12.05,2.5L10.12,2V10.28L5.15,8.95L4.22,6.63L2.77,6.24V11.41L4.37,11.84L9.68,13.27Z" />
	        </svg>
	        <select name="selectArrivee" class="form-control form-control-sm">
	        	<%
	        		out.println("<option selected disabled>Arrivée à</option>");
	        		for(Aeroport aeroport : aeroports){
	        			out.println("<option value='"+aeroport.getId()+"'>"+aeroport.getNom()+"</option>");
	        		}
	        	%>
	        </select>
	      </div>
	      <div id="divDateDepart" class="form-group col-md-6">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M19 3H18V1H16V3H8V1H6V3H5C3.89 3 3 3.89 3 5V19C3 20.1 3.89 21 5 21H19C20.1 21 21 20.1 21 19V5C21 3.89 20.1 3 19 3M19 19H5V8H19V19M12 17V15H8V12H12V10L16 13.5L12 17Z" />
	        </svg>
	        <input type="text" id="dateDepart" name="dateDepart" class="form-control-date">
	      </div>
	      <div id="divDateRetour" class="form-group col-md-6">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M19 3H18V1H16V3H8V1H6V3H5C3.89 3 3 3.89 3 5V19C3 20.1 3.89 21 5 21H19C20.1 21 21 20.1 21 19V5C21 3.89 20.1 3 19 3M19 19H5V8H19V19M12 10V12H16V15H12V17L8 13.5L12 10Z" />
	        </svg>
	        <input type="text" id="dateRetour" name="dateRetour" class="form-control-date">
	      </div>
	      <div id="divClasse" class="form-group col-md-6">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M7 18C7 18 4 10 4 6S6 2 6 2H7C7 2 8 2 8 3S7 4 7 6 10 10 10 13 7 18 7 18M12 17C11 17 8 19.5 8 19.5C7.7 19.7 7.8 20 8 20.3C8 20.3 9 22.1 11 22.1H17C18.1 22.1 19 21.2 19 20.1V19.1C19 18 18.1 17.1 17 17.1H12Z" />
	        </svg>
	        <select name="selectClasse" id="pet-select"class="form-control form-control-sm">
	          <%
	          		ArrayList<Classe> classes = (ArrayList<Classe>) request.getAttribute("classes");
    				out.println("<option selected disabled>Sélectionner une classe</option>");
	          		for(Classe classe : classes){
	          			out.println("<option value='"+classe.getId()+"'>"+classe.getDesignation()+"</option>");
	          		}
	          %>
	        </select>
	      </div>
	      <div class="divPerson form-group col-md-8">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
	        </svg>
	        <select name="person1" id="pet-select"class="form-control form-control-sm">
	          <%
	          		ArrayList<Bareme> baremes = (ArrayList<Bareme>) request.getAttribute("baremes");
	          		out.println("<option selected disabled>Sélectionner une tranche d'age</option>");
	          		for(Bareme bareme : baremes)
	          			out.println("<option value='"+bareme.getId()+"'>"+bareme.getLibelle()+"</option>");
	          %>
	        </select>
	      </div>
	      <div class="divPerson form-group col-md-8">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
	        </svg>
	        <select name="person2" id="pet-select"class="form-control form-control-sm">
	          <%
	          		out.println("<option selected disabled>Sélectionner une tranche d'age</option>");
	          		for(Bareme bareme : baremes)
	          			out.println("<option value='"+bareme.getId()+"'>"+bareme.getLibelle()+"</option>");
	          %>
	        </select>
	      </div>
	      <div class="divPerson form-group col-md-8">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
	        </svg>
	        <select name="person3" id="pet-select"class="form-control form-control-sm">
	          <%
	          		out.println("<option selected disabled>Sélectionner une tranche d'age</option>");
	          		for(Bareme bareme : baremes)
	          			out.println("<option value='"+bareme.getId()+"'>"+bareme.getLibelle()+"</option>");
	          %>
	        </select>
	      </div>
	      <div class="divPerson form-group col-md-8">
	        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
	          <path fill="currentColor"
	            d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
	        </svg>
	        <select name="person4" id="pet-select"class="form-control form-control-sm">
	          <%
	         		out.println("<option selected disabled>Sélectionner une tranche d'age</option>");
	          		for(Bareme bareme : baremes)
	          			out.println("<option value='"+bareme.getId()+"'>"+bareme.getLibelle()+"</option>");
	          %>
	        </select>
	      </div>
	      <div id="divRechercher">
	        <button id="btnRechercher" type="submit" class="btn btn-primary">
	          Rechercher
	        </button>
	      </div>
	    </div>
	</form>
  </div>
</body>
<foot>
	<script>
	    $(function () {
	    	$(".radio").click(function(){
	    		if($("#allerSimple").is(':checked')){
	    			$("#divDateRetour").hide();
					$("#divDateRetour").attr("disabled",true);
	    		}
	    		else{
	    			$("#divDateRetour").show();
	    			$("#divDateRetour").attr("disabled",false);
	    		}
	    	});
	    	$("#dateDepart").datepicker({ dateFormat: 'yy-mm-dd' });
	        $("#dateRetour").datepicker({ dateFormat: 'yy-mm-dd' });
		});
	  </script>
</foot>
</html>