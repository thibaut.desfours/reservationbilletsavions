<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.rba.beans.Reservation" %>
<%@ page import="com.rba.beans.Billet" %>
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="css/jquery-ui.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/recapitulatifReservation.css">
  <link rel="stylesheet" type="text/css" href="css/recapitulatifReservation.css"/>
  <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>  
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <title>Récapitulatif de la réservation</title>
</head>

<body>
	<%@include file="header.jsp" %>
	<div class="container" style="padding-top: 5em;">
		<h1>Récapitulatif de la réservation</h1>
		<table class="table">
		 	<thead>
		    	<tr>
			      <th scope="col">Nom</th>
			      <th scope="col">Prénom</th>
			      <th scope="col">Tranche d'age</th>
			      <th scope="col">Classe</th>
			      <th scope="col">Trajet</th>
			      <th scope="col">Bagage en soute</th>
			      <th scope="col">Prix</th>
			    </tr>
		  	</thead>
		  	<tbody>
		  		<%
		  			Reservation reservation = (Reservation) session.getAttribute("reservation");
		  			for(Billet billet : reservation.getBillets()){
		  		%>
				    	<tr>
				    		<td><%out.println(billet.getNom());%></td>
				    		<td><%out.println(billet.getPrenom());%></td>
				    		<td><%out.println(billet.getBareme().getLibelle());%></td>
				    		<td><%out.println(billet.getClasse().getDesignation());%></td>
				    		<td><%out.println(billet.getVol().getAeroportD().getVille()+" -> "+billet.getVol().getAeroportA().getVille());%></td>
				    		<td><%
				    			float bagage;
				    			if(billet.isBagage()){
				    				out.println("Oui");
				    				bagage = 50;
				    			}				    				
				    			else{
				    				out.println("Non");
				    				bagage = 0;
				    			}
				    		%></td>
				    		<td><%out.println((billet.getVol().getPrix_base()*billet.getClasse().getCoefficient())*billet.getBareme().getCoefficient()+bagage);%></td>
					    </tr>
			    <%
		  			}
			    %>
			</tbody>
		</table>
		<div class="row">
			<button type="button" class="btn btn-warning col-sm-6" onclick="javascript:history.go(-1);">Précédent</button>
			<button type="button" class="btn btn-success col-sm-6" onclick="document.location.href='paiement';">Paiement</button>
		</div>
	</div>	
</body>
