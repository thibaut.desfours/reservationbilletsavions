<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.rba.beans.Classe" %>
<%@ page import="com.rba.beans.Aeroport" %>
<%@ page import="com.rba.beans.Bareme" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="css/jquery-ui.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/confirmationPaiement.css"/>
  <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>  
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <title>Confirmation du paiement</title>
</head>

<body onload="setTimeout(function(){document.location = 'reservation'}, 5000)">
	<div id="confirm" class="swal2-icon swal2-success swal2-animate-success-icon" style="display: flex;">
 		<div class="swal2-success-circular-line-left" style="background-color: rgb(255, 255, 255);"></div>
   		<span class="swal2-success-line-tip"></span>
   		<span class="swal2-success-line-long"></span>
   		<div class="swal2-success-ring"></div> 
   		<div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>
   		<div class="swal2-success-circular-line-right" style="background-color: rgb(255, 255, 255);"></div>
  	</div>
  	<div id="msgBox">
  		<div id="paymentConfirm">Confirmation du paiement</div>
  		<div id="redirectionMsg">Vous allez être redirigé vers la page d'accueil</div>
  	</div>
  	<div class="dot-pulse"></div>
</body>
</html>