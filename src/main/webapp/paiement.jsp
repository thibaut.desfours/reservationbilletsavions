<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.rba.beans.Reservation" %>
<%@ page import="com.rba.beans.Billet" %>
<%@ page import="java.text.DateFormat" %><%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %><%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.concurrent.TimeUnit" %><%@ page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">

	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/paiement.css"/>
 	<link rel="stylesheet" href="css/jquery-ui.min.css">
 	<link rel="stylesheet" href="css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="css/paiement.css"/>
 	<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
 	<script type="text/javascript" src="js/jquery-ui.min.js"></script>  
 	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<title>Modalité de paiement</title>
</head>
<body>
	<%@include file="header.jsp" %>
	<div class="container" style="padding-top: 5em;">
		<div class="form-row">
			<h1>Montant total</h1>
			<table class="table">
			 	<thead>
			    	<tr>
				      <th scope="col">Nom</th>
				      <th scope="col">Prénom</th>
				      <th scope="col">Tranche d'age</th>
				      <th scope="col">Classe</th>
				      <th scope="col">Trajet</th>
				      <th scope="col">Bagage en soute</th>
				      <th scope="col">Prix (€)</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		<%
			  			Reservation reservation = (Reservation) session.getAttribute("reservation");
			  			float prixTotal = 0;
			  			for(Billet billet : reservation.getBillets()){
			  		%>
						    <tr>
					    		<td><%out.println(billet.getNom());%></td>
					    		<td><%out.println(billet.getPrenom());%></td>
					    		<td><%out.println(billet.getBareme().getLibelle());%></td>
					    		<td><%out.println(billet.getClasse().getDesignation());%></td>
					    		<td><%out.println(billet.getVol().getAeroportD().getVille()+" -> "+billet.getVol().getAeroportA().getVille());%></td>
					    		<td><%
					    			float bagage;
					    			if(billet.isBagage()){
					    				out.println("Oui");
					    				bagage = 50;
					    			}				    				
					    			else{
					    				out.println("Non");
					    				bagage = 0;
					    			}
					    		%></td>
					    		<td><%
					    			float prixLigne = (billet.getVol().getPrix_base()*billet.getClasse().getCoefficient())*billet.getBareme().getCoefficient()+bagage;
					    			prixTotal += prixLigne;
					    			out.println(prixLigne);
					    			%></td>
						    </tr>						    
				    <%
			  			}
				    %>
				    	<tr>
			  				<td>Total net à payer</td>
			  				<td></td>
			  				<td></td>
			  				<td></td>
			  				<td></td>
			  				<td></td>
			  				<td><%out.println(prixTotal);%></td>
			  			</tr>
				    	
				</tbody>
			</table>
		</div>
		
		<form method="post" action="confirmationPaiement">     
			<div class="form-row" id="partiecentrer">
				<div class="form-row">
					<h1>Modalité de paiement</h1>
				</div>
				<div class="form-group col-md-12">
			     	<label for="num_carte">Numéro de la carte :</label>
			     	<input type="tel" pattern="[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}" maxlength="19" class="form-control" id="num_carte" name="num_carte" placeholder="8552 **** **** ****" required>			     	
			    </div>
			    <div class="form-group col-md-6">
			    	<label for="nom_carte">Nom du porteur :</label>
			     	<input type="text" class="form-control" id="nom_carte" name="nom" placeholder="BADOU" required>
			    </div>
			    <div class="form-group col-md-6">
			    	<label for="prenom_carte">Prénom du porteur :</label>
			     	<input type="text" class="form-control" id="prenom_carte" name="prenom" placeholder="Jérémy" required>
			    </div><br>
			    <div class="form-group col-md-6">
			    	<label for="tel">Numéro de téléphone du porteur :</label>
			     	<input type="text" class="form-control" id="tel" name="tel" placeholder="0652645987" required>
			    </div>
			    <div class="form-group col-md-6">
			    	<label for="mail">Adresse email du porteur :</label>
			     	<input type="text" class="form-control" id="email" name="email" placeholder="jeremy.badou@exemple.fr" required>
			    </div><br>
			    <div class="form-group col-md-4">
			     	<label for="date_expiration">Date d'expiration :</label>
			     	<input type="tel" pattern="[0-9]{2}/[0-9]{2}" maxlength="5" class="form-control" id="date_expiration" name="date_expiration" placeholder="MM/AA" required>
			     	
			    </div>
			    <div class="form-group col-md-4">
			     	<label for="code_securite">Code de sécurité :</label>
			     	<input type="tel" class="form-control" pattern="[0-9]{3}" maxlength="3" id="code_securite" name="code_securite" required>  	
			     	
			    </div>
			</div>
			<div class="row"  id="partieButton">
				<button type="button" class="btn btn-warning" onclick="javascript:history.go(-1);">Précédent</button>&nbsp;&nbsp;&nbsp;
				<button type="submit" class="btn btn-success">Valider paiement</button>
			</div>
		</form>
	</div>
</body>
</html>