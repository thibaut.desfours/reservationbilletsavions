<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="java.text.DateFormat" %><%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %><%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.concurrent.TimeUnit" %><%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="java.util.Locale" %><%@ page import="java.util.Locale" %>
<%@ page import="com.rba.beans.Vol" %>
<%@ page import="com.rba.beans.Classe" %>
<%@ page import="com.rba.beans.Aeroport" %>
<%@ page import="com.rba.beans.Bareme" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<link rel="stylesheet" type="text/css" href="css/vols.css"/>
	<link rel="stylesheet" href="css/jquery-ui.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>  
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<title>Liste des vols</title>
</head>

<body>
	<%@include file="header.jsp" %>
    <h1>
    	<% 
    		Aeroport aeroportD = (Aeroport) request.getAttribute("aeroportD");
    		Aeroport aeroportA = (Aeroport) request.getAttribute("aeroportA");
    		out.println(aeroportD.getNom()+" => "+aeroportA.getNom()); 
    	%>
    </h1>
    <div class="entete">
        <div class="flightCardContent">
            <p class="trajet">Trajet</p>
            <p class="dateDepart">Date de départ</p>
            <p class="tempsVol">Temps de vol</p>
            <p class="prix">Prix</p>
    	</div>
    </div>
    <div class="line"></div>
    <div id="flights">
    	<%
    		ArrayList<Vol> vols = (ArrayList<Vol>) request.getAttribute("vols");
    		ArrayList<Bareme> baremes = (ArrayList<Bareme>) request.getAttribute("baremes");
    		Classe classe = (Classe) request.getAttribute("classe");
    		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.FRENCH);
    		for(Vol vol : vols){
    			float prixVol = 0;
    			for(Bareme bareme : baremes)
    				prixVol += (vol.getPrix_base()*classe.getCoefficient())*bareme.getCoefficient();
    			
    			long dureeTrajetMillis = Math.abs(format.parse(vol.getDateD()).getTime() - format.parse(vol.getDateA()).getTime());
    			long dureeTrajetHeure = TimeUnit.HOURS.convert(dureeTrajetMillis, TimeUnit.MILLISECONDS);
    			
    			out.println("<div class='flightCard'>");
    				out.println("<div class='flightCardContent'>");
   					out.println("<p class='trajet'>"+aeroportD.getNom()+" => "+aeroportA.getNom()+"</p>");
   					out.println("<p class='dateDepart'>"+vol.getDateD()+"</p>");
   					out.println("<p class='tempsVol'>"+dureeTrajetHeure+" h</p>");
   					out.println("<div class='prix'>");
   					String typeVol = (String) request.getAttribute("typeVol");
   					if(request.getAttribute("volRetour") != null)
   						out.println("<button id='btnRechercher' type='button' onclick='document.location.href=\"ajoutbillet?volRetour="+vol.getId()+"\"'>Prix : "+prixVol+" €<br>Réserver ></button>");
   					else if(typeVol.equals("Aller simple"))
   						out.println("<button id='btnRechercher' type='button' onclick='document.location.href=\"ajoutbillet?volAller="+vol.getId()+"\"'>Prix : "+prixVol+" €<br>Réserver ></button>");
   					else if(typeVol.equals("Aller-retour"))
   						out.println("<button id='btnRechercher' type='button' onclick='document.location.href=\"vols?volAller="+vol.getId()+"\"'>Prix : "+prixVol+" €<br>Réserver ></button>");
   					out.println("<button id='btnDetails' type='button' onclick='document.location.href=\"details?vol="+vol.getId()+"\"'>Voir détails</button></div></div></div>");
    		}
    	%>     
    </div>
</body>
</html>