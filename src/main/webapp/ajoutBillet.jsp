<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.rba.beans.Reservation" %>
<%@ page import="com.rba.beans.Billet" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/ajoutBillet.css"/>
	<link rel="stylesheet" href="css/jquery-ui.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>  
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
<title>Création des billets</title>
</head>
<body>
	<%@include file="header.jsp" %>
	<form method="post" action="recapitulatif">
		<% 
			Reservation reservation = (Reservation) session.getAttribute("reservation");
			int i =0;
			for(Billet billet : reservation.getBillets()){
		%>
				<div class="person">
					<div>
						<label id="label"><%
						if(i == 0)
							out.println("1er Billet - "+billet.getBareme().getLibelle()+" - "+billet.getVol().getAeroportD().getVille()+" -> "+billet.getVol().getAeroportA().getVille());
						else
							out.println((i+1)+"ème Billet - "+billet.getBareme().getLibelle()+" - "+billet.getVol().getAeroportD().getVille()+" -> "+billet.getVol().getAeroportA().getVille());
						%></label>
					</div>
					<div class="line"></div>
					<div>
						<label>Civilité : </label>
					</div>
					<div>
					  	<label for="M">M</label>
					  	<input type="radio" <%out.println("name='civilite"+i+"'");%> value="M" checked>
					  	<label for="Mme">F</label>
					  	<input type="radio" <%out.println("name='civilite"+i+"'");%> value="F">
					</div>
					<div>
					  	<label for="nom">Nom : </label>
					  	<input type="text" id="nom" <%out.println("name='nom"+i+"'");%>>
					</div>
					<div>
					  	<label for="prenom">Prénom : </label>
					  	<input type="text" id="prenom" <%out.println("name='prenom"+i+"'");%>>
					</div>
					<div>
						<label>Bagages en soute ? </label>
					  	<label for="Oui">Oui</label>
					  	<input type="radio" id="oui" <%out.println("name='bagages"+i+"'");%> value="true" checked>
					  	<label for="Non">Non</label>
					  	<input type="radio" id="non" <%out.println("name='bagages"+i+"'");%> value="false">
					</div>
				</div>
	    <% 
	    	i++;
	    	} 
	    %>
	    <div class="buttons">
	    	<button type="button" id="btnRetour" onclick="javascript:history.go(-1);">Retour</button>
	    	<input type="submit" id="btnContinuer" value="Continuer">
	    </div>
	</form>
	
</body>
</html>