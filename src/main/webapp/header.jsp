<%@page contentType="text/html"
        pageEncoding="UTF-8"
%>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1faefa;">
  <a class="navbar-brand" href="reservation"><img src="images/logo.jpg" style="width: 70px; height: auto;"/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Mes réservations <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Acheter billet</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Informations</a>
      </li>
    </ul>
  </div>
</nav>