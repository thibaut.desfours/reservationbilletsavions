<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="java.text.DateFormat" %><%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %><%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.concurrent.TimeUnit" %><%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="java.util.Locale" %><%@ page import="java.util.Locale" %>
<%@ page import="com.rba.beans.Vol" %>
<%@ page import="com.rba.beans.Reservation" %>

<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
    <link rel="stylesheet" type="text/css" href="css/details.css"/>
  	<link rel="stylesheet" href="css/jquery-ui.min.css">
 	<link rel="stylesheet" href="css/bootstrap.min.css">
  	<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
  	<script type="text/javascript" src="js/jquery-ui.min.js"></script>  
  	<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <title>Les détails du vol</title>
</head>

<body>
    <%@include file="header.jsp" %>
	<h1 id="title">Détails du vol</h1>
	<div class="entete">
        <div class="flightCardContent">
            <p class="depart">Départ</p>
            <p class="arrivee">Arrivée</p>
            <p class="duree">Durée</p>
            <p class="referenceVol">Référence de vol</p>
            <p class="typeAvion">Type d'avion</p>
        </div>
    </div>
    <div class="line"></div>
    <div id="flights">
    	<%
			Vol vol = (Vol) request.getAttribute("vol");
    		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.FRENCH);
	    	long dureeTrajetMillis = Math.abs(format.parse(vol.getDateD()).getTime() - format.parse(vol.getDateA()).getTime());
			long dureeTrajetHeure = TimeUnit.HOURS.convert(dureeTrajetMillis, TimeUnit.MILLISECONDS);
    	%>
        <div class="flightCard">
            <div class="flightCardContent">
                <p class="depart"><%out.println(vol.getAeroportD().getNom());%></p>
                <p class="arrivee"><%out.println(vol.getAeroportA().getNom());%></p>
                <p class="duree"><%out.println(dureeTrajetHeure+" h");%></p>
                <p class="referenceVol"><%out.println(vol.getId());%></p>
                <p class="typeAvion"><%out.println(vol.getAvion().getModele());%></p>
            </div>
        </div>   
    </div>
    <div class="buttons">
    	<button type="button" id="btnRetour" onclick="javascript:history.go(-1);">Retour</button>
    </div>
</body>
</html>